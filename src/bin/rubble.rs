use std::io;

use rubbler::rubble;

fn main() {
    println!("> Input RISC-V assembly line(s): (Press <CTRL-d> once finished)");
    let mut source = String::new();
    loop {
        let mut line = String::new();
        if io::stdin().read_line(&mut line).unwrap() == 0 {
            break;
        }
        source += &line;
    }
    println!("> Rubbling...");
    match rubble(source.as_str()) {
        Ok(bytes) => {
            println!("> Here's your bytes:");
            for (i, byte) in bytes.iter().enumerate() {
                if i != 0 && i % 4 == 0 {
                    println!()
                }
                print!("{:08b}", byte)
            }
            println!();
        }
        Err(e) => println!("{}", e),
    }
}
