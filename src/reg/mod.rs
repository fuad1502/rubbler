use std::{collections::HashMap, sync::OnceLock};

static REGISTERS: OnceLock<Registers> = OnceLock::new();

pub struct Registers {
    map: HashMap<&'static str, u32>,
}

impl Registers {
    fn new() -> Registers {
        let mut map = HashMap::new();
        map.insert("zero", 0);
        map.insert("ra", 1);
        map.insert("sp", 2);
        map.insert("gp", 3);
        map.insert("tp", 4);
        map.insert("t0", 5);
        map.insert("t1", 6);
        map.insert("t2", 7);
        map.insert("s0", 8);
        map.insert("fp", 8);
        map.insert("s1", 9);
        map.insert("a0", 10);
        map.insert("a1", 11);
        map.insert("a2", 12);
        map.insert("a3", 13);
        map.insert("a4", 14);
        map.insert("a5", 15);
        map.insert("a6", 16);
        map.insert("a7", 17);
        map.insert("s2", 18);
        map.insert("s3", 19);
        map.insert("s4", 20);
        map.insert("s5", 21);
        map.insert("s6", 22);
        map.insert("s7", 23);
        map.insert("s8", 24);
        map.insert("s9", 25);
        map.insert("s10", 26);
        map.insert("s11", 27);
        map.insert("t3", 28);
        map.insert("t4", 29);
        map.insert("t5", 30);
        map.insert("t6", 31);
        Registers { map }
    }
    pub fn get_number(name: &str) -> Option<&u32> {
        let registers = REGISTERS.get_or_init(|| Registers::new());
        registers.map.get(name)
    }
    pub fn is_reg(name: &str) -> bool {
        let registers = REGISTERS.get_or_init(|| Registers::new());
        match registers.map.get(name) {
            Some(_) => true,
            None => false,
        }
    }
}
