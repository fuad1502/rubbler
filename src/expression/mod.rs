pub struct Expression {
    expr_type: ExprType,
    line_number: i32,
}

pub enum ExprType {
    NumberLiteral(i32),
    RegisterLiteral(u32),
    StringLiteral(String),
    MemAddrLiteral(u32, MemOffset),
    Symbol(String),
}

pub enum MemOffset {
    Number(i32),
    Symbol(String),
}

impl Expression {
    pub fn new_num(n: i32, line_number: i32) -> Expression {
        Expression {
            expr_type: ExprType::NumberLiteral(n),
            line_number,
        }
    }
    pub fn new_reg(r: u32, line_number: i32) -> Expression {
        Expression {
            expr_type: ExprType::RegisterLiteral(r),
            line_number,
        }
    }
    pub fn new_str(s: String, line_number: i32) -> Expression {
        Expression {
            expr_type: ExprType::StringLiteral(s),
            line_number,
        }
    }
    pub fn new_memaddr(r: u32, off: MemOffset, line_number: i32) -> Expression {
        Expression {
            expr_type: ExprType::MemAddrLiteral(r, off),
            line_number,
        }
    }
    pub fn new_sym(s: String, line_number: i32) -> Expression {
        Expression {
            expr_type: ExprType::Symbol(s),
            line_number,
        }
    }
    pub fn get_type(&self) -> &ExprType {
        &self.expr_type
    }
}
