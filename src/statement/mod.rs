use crate::expression::Expression;

pub struct Statement {
    line_number: i32,
    stmt_type: StmtType,
}

pub enum StmtType {
    Label(Vec<String>),
    Operation(String, Vec<Expression>),
    Directive(String, Vec<Expression>),
}

impl Statement {
    pub fn new_label(labels: Vec<String>, line_number: i32) -> Statement {
        Statement {
            line_number,
            stmt_type: StmtType::Label(labels),
        }
    }
    pub fn new_operation(op: String, args: Vec<Expression>, line_number: i32) -> Statement {
        Statement {
            line_number,
            stmt_type: StmtType::Operation(op, args),
        }
    }
    pub fn new_directive(dir: String, args: Vec<Expression>, line_number: i32) -> Statement {
        Statement {
            line_number,
            stmt_type: StmtType::Directive(dir, args),
        }
    }
    pub fn get_type(&self) -> &StmtType {
        &self.stmt_type
    }
    pub fn get_line_number(&self) -> i32 {
        self.line_number
    }
}
