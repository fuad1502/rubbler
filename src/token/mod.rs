pub struct Token {
    lexeme: String,
    token_type: TokenType,
    line_number: i32,
}

impl Token {
    pub fn new(lexeme: String, token_type: TokenType, line_number: i32) -> Token {
        Token {
            lexeme,
            token_type,
            line_number,
        }
    }

    pub fn new_number(lexeme: String, literal: i32, line_number: i32) -> Token {
        Token {
            lexeme,
            token_type: TokenType::Number(literal),
            line_number,
        }
    }

    pub fn new_reg(lexeme: String, reg: u32, line_number: i32) -> Token {
        Token {
            lexeme,
            token_type: TokenType::Register(reg),
            line_number,
        }
    }

    pub fn get_lexeme(&self) -> String {
        self.lexeme.clone()
    }

    pub fn get_type(&self) -> TokenType {
        self.token_type
    }

    pub fn get_line_number(&self) -> i32 {
        self.line_number
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TokenType {
    // Single character tokens
    LeftParantheses,
    RightParantheses,
    Colon,
    Comma,
    LineBreak,

    // Multi character token
    Identifier,
    Number(i32),
    String,
    Opcode,
    Register(u32),
    Directive,
}
