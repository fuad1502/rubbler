pub fn error(line_number: i32, what: &str, description: &str) -> String {
    "[Line ".to_string() + &(line_number + 1).to_string() + "] " + what + ": " + description
}
