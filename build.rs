use std::env;

fn main() {
    if env::var("DOCS_RS").is_err() {
        if let Ok(crate_dir) = env::var("CARGO_MANIFEST_DIR") {
            cbindgen::Builder::new()
                .with_crate(crate_dir)
                .generate()
                .expect("Unable to generate bindings")
                .write_to_file("target/rubbler.h");
        }
    }
}
